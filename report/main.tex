%
% Final report for Ec/CS101 group project
% Starts with the exact text from the proposal
%
\documentclass[12pt]{article}

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{fullpage}
\usepackage{hyperref}

\title{A Novel Approach to Cost Minimizing Surveys when Cost is Correlated to Data}
\author{Group: Value of Private Information\\Members: Arjun Chandrasekhar, Solomon Chang, Bryan He, \\Eric Martin, Brian Okoro, Kevin Shi\\Facilitator: Zhenhua Liu}

\begin{document}
\maketitle

\section{Introduction}

Only recently has academia been able to develop and analyze a rigorous definition for data privacy.  One ongoing area of research is to determine the economic value of privacy and private information.  In particular, is there a way to quantify how individuals value their information and the ability to keep their information private?  What sort of incentives (both in terms of compensation and privacy guarantee) are required for a second party to obtain sensitive information?  The benefit of this knowledge is simple: it will allow analysts to obtain useful and valuable information in an efficient, cost-effective manner.  

Importantly, individuals have different valuations on their private information. The analyst must buy or otherwise obtain this information from a representative sample of the population of interest. We immediately see the tradeoffs for setting prices for private information. If the analyst underpays, he probably wouldn’t obtain repreentative data because he is likely to miss out on the most sensitive types who want high payoffs for disclosure; if he overpays he compromises his resource allocation.  Thus it is very important to design a model/algorithm for efficiently trading private information and maximizing the utilities of both the analyst and the participants.

This work aims to tackle this problem by using methods in statistics, machine learning, and economics to create an efficient auction algorithm that pays users for their private information. It provides an intuitive, standardized mechanism for facilitating the trade of private information while reducing the cost to the data collector. The mechanism generates statistically valid estimates for the distributions of users' utilities for their private information. The described approach also relaxes prior assumptions about users’ private utility functions, namely allowing correlation between an individual's private information and their valuation for that information. 

\section{Background}
% condense background info from proposal

The most widely-used definition of privacy is $\epsilon$-differential privacy.  Intuitively, a mechanism for producing output to a query on a database is $\epsilon$-differentially private if the presence of one person’s data point increases the chance that the mechanism produces a specific output by at most a factor of $e^{\epsilon}$ -- i.e. if an individual chooses to allow his/her data to be in the database, the risk that a query results in an output the individual might find unfavorable is increased by a limited factor.

Currently, researchers are encountering issues when attempting to find out how much incentive is necessary to coax information out of an individual who is given the guarantee of a specific level of privacy.  The naive approach might be to hope that individuals will reveal their (possibly nondeterministic) utility functions (the amount of payoff required to reveal their information as a function of $\epsilon$, and possibly other parameters).  The problem is that if one’s cost function is correlated with his/her private type, then revealing one’s true cost function could necessarily compromise privacy.  Thus the challenge lies in being able to find incentives for individuals to reveal data that is consistent enough with their cost functions so as to allow acquisition of useful data without compromising privacy.  The following represents a sample of some prior work in the area:

Auction mechanisms have been studied in the context of markets for private information. In “Selling Privacy at Auction” by Ghosh and Roth, the classical Vickrey and multiple procurement auction mechanisms are shown to enable both accurate and differentially private pricing in private information sales~\cite{Ghosh2011}. This work is particularly applicable to a party that needs to purchase private information, and is either seeking some fixed accuracy goal (and wants to spend the least money possible) or the highest quality dataset for some fixed budget. In this work, an individual's ``private type,'' a measure of how much they value their private information, is assumed to be independent of the content of that private information. A private type can be regarded as an individual’s utility as a function of the amount of privacy that he or she gives up, and individuals can be classified based on their private type. 

In “Conducting Truthful Surveys, Cheaply”, the authors consider the related problem where a third party attempts to obtain an estimate of a population statistic while minimizing certain degrees of freedom~\cite{Roth2012}.  They base their work on the assumption that individual costs are unknown, but drawn from a known prior.  This is the basis of their work in which they derive truthful mechanisms for payment schemes that fix or vary the two degrees of freedom: variance of estimation and survey cost. The model also assumes that private type and response rate are independent of each other.

Fleischer and Lyu address this particularly limiting assumption in “Approximately optimal auctions for selling privacy when costs are correlated with data.”~\cite{Fleischer2012}. They develop a model in which individuals are offered contracts which specify payouts for certain private types. Individuals can either accept or reject the contract, with acceptance leading to their private types being verified and the individuals being paid the amounts dictated by the contracts. The model is shown to create a privacy-preserving information auction that also approximates the optimal market state.  One notable aspect of the model is that it makes specific assumptions about the underlying distributions from which users’ cost functions are drawn.

The results of auction mechanisms on markets for private information are studied in “Privacy and coordination: Computing on databases with endogenous participation”~\cite{Ghosh2013}. In this paper, Ghosh and Ligett consider a model where a privacy-sensitive population with individual privacy requirements is allowed to decide whether or not to participate in a specific noisy computation. As a result of the ability to decide whether or not to participate in the computation, the database is endogenously determined by the choices made by the individuals. This paper reaches the main conclusion that the analyst can actually increase the accuracy of the result by adding more noise due to the fact that more individuals may be willing to participate in the database when the computation is more noisy.


\section{Objectives}
% specific objectives and goals

The primary objective is to use reinforcement learning techniques to learn what costs individuals associate with revealing their particular private type (and by extension, their private information). Namely, we assume that for each private type there is a probability distribution for the utility that an individual of said private type receives for revealing their type. Furthermore, we want to minimize the amount of money spent paying individuals for their private type while maximizing information gain regarding each private type’s utility distribution. The performance of our mechanism will be quntified and compared to previous mechanisms and other buying strategies.

This is highly relevant to other work in the field because it relaxes the prior assumptions about the utility functions for private information found in previous work. This empirical method for determining the underlying utility functions will allow other work to generate more quantitatively useful privacy valuations.


\section{Approach}

\subsection{Simulated Data}

Reinforcement learning algorithms such as ours are dependent on the past actions of the algorithm and the inviduals' responses to those actions. As a result, it is not sufficient to use a data set that contains only the private type and the payment that each individual accepted to reveal their private type; instead, the data needed to evaluate the algorithm must include a numerical value for each individual that represents the value that they place on their private type.   By obtaining this numerical value, we may accurately determiine individual responses to our algorithm's offers. Since this type of numerical data would be difficult to obtain, we shall use simulated data instead.

% details on simulated data: 2 private types, their parameterized distributions
Although any number of private types may exist in theory, it is useful to consider the case of two distinct types. These types correspond to a subpopulation with a low valuation of privacy and a subpopulation with a high valuation of privacy.  Therefore, each private type is associated with a distribution which ranges over the possible privacy valuations.  This distribution represents the composition of valuations within the corresponding private type. Each individual belonging to the population can be classified as one of the two types with a privacy valuation drawn from their respective private type distribution.


\subsection{Learning Algorithm}
% see readme
% make mathematical notation, figures

We attempt to learn the utility distributions for each private type using a variety of supervised and reinforcement learning techniques. For each successive individual, our algorithm chooses a price to offer an individual for their private information.

The nonparametric kernel density estimation (KDE) technique is used to estimate the utility distributions. The mechanism receives an individual's response to a bid (Accept, Reject) and updates the distributions based on this new information. An accepted offer places an upper bound on an individual's valuation -- the individual accepted an offer above her valuation. Similarly, a rejected offer places a lower bound on an individual's valuation. For each private type, a Gaussian kernel is centered at a mean sampled from the current learned distribution satisfying the above bounds. Each kernel is weighted by the probability that the individual is in each private type (weight is 1 when an offer is Accepted and the private type is confirmed).

\subsection{Bid Offering Strategy}
% 1 bid/person, random value
% multiple increasing bids, sampling strategy

This section describes various strategies for selecting the amount to offer to the next individual.
The more basic strategies give a single offer, and after the individual accepts or rejects an offer, no further offers are made.
Single offer strategies cause truthfulness to be a dominating strategy for the individual revealing their valuation.
A more sophisticated strategy allows multiple offers to be made.
If an individual accepts an offer, no further offers are made, but if an offer is rejected, then an increased offer may be made with a given probability.
Note that if the increased offer is not made, the individual may not accept the old offer anymore.
Multiple offers allows more information to be gained; however, truthfulness is no longer a dominating strategy.
An upper bound for the degree of untruthfulness is derived in Section \ref{section:truth_bound}

%We draw from truthful survey mechanisms presented in the literature, as well as principles from machine learning, game theory, and Bayesian analysis to develop a bid offering strategy that effectively procures accurate data under a cost constraint or a limited number of trials. We implement a preliminary mechanism along with describing potential avenues for developing a more sophisticated mechanism, which while being more difficult to implement could provide a more robust reinforcement to our learning algorithm. We analyze the Bayesian incentive truthfulness of this more sophisticated mechanism, providing a mathematical discussion of the parameters which affect the survey's truthfulness in the context of a repeated Bayesian game.

\subsubsection{Single Offer, Maximum Privacy Valuation}

Consider the case where the maximum privacy valuation is offered on every bid. The individual always accepts this bid because it is greater than her valuation. Each bid in this case only gives the mechanism a weak upper bound on this quantity.  In the following results section, we shall utilize the maximum privacy valuation offer strategy as a baseline against the other suggested offer strategies.

\subsubsection{Single Offer, Randomly Selected}

Each person is given only one offer which is accepted or rejected. Randomly picking offers elicit a truthful response. Making single random offers improves upon the maximum offer strategy by covering the entire range of bids, allowing stronger bounds on valuations.  Random offers produces stronger bounds because the mechanisms is able to accumulate a variety of upper bounds on private type valuations, rather than the singular weak upper bound of the maximum privacy valuation.

\subsubsection{Single Offer, Based on Learned Distributions}

This section describes offer strategies that depend on the learned distributions.
%Again, only a single offer is made. However, instead of randomly selecting a bid for each person, we choose a bid that is a function of the currently learned distributions.


\paragraph{Uniform Type Sample}

At any give time step, we may sample our current learned distributions uniformly across the private types.  Therefore, the learned distributions at each time step defines the next mechanism offer.

\paragraph{Most Prevalent Type Sample}

We may adapt this above method as follows to instead sample the current learned distribution of the most common private type.  Again, the learned distribution at each time step defines the next mechanism offer. This mechanism has bias to low valuations, especially if the population is predominately low valuation individuals: low valuations are selected with higher frequency, and therefore high valuation individuals will reject the offer.

\paragraph{Least Prevalent Type Sample}

Again we may adapt this above method to sample the current learned distribution of the least common private type.  Again, the learned distsribution at each time step defines the next mechanism offer.  This mechanism has bias to low valuations, especially if the population is predominately low valuation individuals as described in the previous section.


\subsection{Multiple Offers}

Strategies that incorporate multiple offers should perform even better, again by providing stronger bounds on valuations. Multi-offer strategies also allow both lower and upper bounds to be determined for an individual's valuation.  While offering monotonically increasing offers for an individuals private type, if the individual eventually accepts an offer, then the mechanism can recall the second to last and the final offer in order to gather a lower and an upper bound on the individual's valuation.  If the mechanism terminates its sequence of monotonically increasing offers before the individual accepts an offer, the mechanism gains no additional information than the simple single offer strategies; namely, the mechanism solely learns the upper bound of the individual's valuation.

\subsection{Truthfulness Bound}
\label{section:truth_bound}

The single offer strategies presented here are truthful, i.e. the individual will accept offers at or above their true valuation and reject otherwise. Since only a single offer is made, the individual cannot ``hold out'' for a larger payoff.

However, the multiple increasing offer strategy does not elicit truthful acceptance strategies on the part of the individuals selling their private information. Intuitively, when an individual is offered some amount of money above their valuation, she can either accept immediately (truthful) or reject in hope of getting a higher offer (untruthful). A mechanism that makes new offers (1) with high probability and (2) of much larger value should elicit more untruthful acceptance behavior. We now establish bounds on the degree of untruthfulness as a function of these two factors.

% figure for numberline showing relative positions of v, b1, b2, alpha, epsilon

First, note that if the initial offer is below the individual's valuation, the individual should always reject the bid. Continue making increasing offers until an offer $b_1$ is greater than the individual's valuation $v$. The probability of making a new offer is $\alpha$ and ending the bidding is $1-\alpha$.

The individual's expected utility for truthfulness:

$$\mathbb{E}(b_1) = \epsilon$$

must be at least her expected utility for untruthfulness:

$$\mathbb{E}(b_2) = (1-\alpha)(0) + \alpha(b_2-v)$$

This occurs when

$$\alpha(b_2-v) \leq \epsilon$$

Noticing that $b_1 = v + \epsilon$, we are left with

$$b_2 \leq b_1 + \epsilon \left( \frac{1}{\alpha}-1 \right)$$

where $\epsilon \left( \frac{1}{\alpha}-1 \right)$ is the maximum amount that the offer can be increased by to ensure truthfulness given $\alpha$ probability of offering a new bid and $\epsilon$ difference between the first offer and the individual's actual valuation. The value of $\epsilon$ can become arbitrarily large, but it's expectation can be calculated from the current learned distribution. We can also implement the reverse, choosing increment $k = b_2 - b_1$ and $\alpha$ such as to bound the value of untruthfulness and thus predict to what extent players will lie. 

For instance, suppose we would like to bound the value of untruthfulness to \$1, i.e. incentivize players to take any offer which exceeds their privacy valuation by at least \$1. Then we would like to select $k$, $\alpha$ such that

$$ \frac{k}{\frac{1}{\alpha}-1} < \$1 $$

$(k,\alpha) = (\$1, 0.5 - \delta)$, where $\delta$ is a small value, is an example of a tuple satisfying this condition. Thus we show that, using small offer increments, we can implement a multiple bid strategy that provides useful learning data without introducing a high level of untruthfulness. 

\subsection{Quantifying Estimator Quality}

A good estimator must learn both the distributions of valuations within each private type and the proportion of individuals falling into each private type while also remaining budget feasible.  To create a single metric/objective function for quantifying the overall ``closeness'' of the learned model to the underlying distribution, we use Jenson-Shannon (J-S) divergence.  J-S divergence represents a single metric that encapsulates how close a distribution is to what it ``should'' be -- in our case, how close the learned distribution is to the actual distribution for a given private type.  The J-S divergence is a smoothed and symmetric version of the usual Kullback-Leibler (K-L) divergence with properties well suited to our numerical implementation. These include numerical stability at boundary conditions (i.e. if one of the distributions has a probability density function of 0 at a certain point which causes the K-L divergence to become unbounded) and  bounds on the interval $[0,1]$. The J-S divergence between the learned model and the underlying distribution for each private is calculated. These values are weighted by the underlying proportion of individuals in each private type and summed to create a measure of our learning mechanism's effectiveness.

Testing the budget feasibility of or mechanism is simply a matter of plotting and comparing cumulative cost over time.  As our data shows, our learning mechanisms manage to be relatively good at avoiding overpaying for individual data.


\section{Results and Discussion}
% What did you actually manage to do? Describe any results, models, implementations, experiments, etc. in detail.
% What aspects of your central question does this work address? What aspects of the question doesn't it address? What alternate approaches could you have tried?

\subsection{Estimation Quality Comparison}
% 1000 people seems like a reasonable amount
The quality of the learned distribution was calculated for increasing numbers of samled individuals (Figure~\ref{fig:strategy_log_divergences}).

\begin{figure}
  \centering
  \includegraphics[width=0.7\textwidth]{strategy_log_divergences}
  \caption{\label{fig:strategy_log_divergences} Change in J-S divergence for increasing number of sampled individuals for different offer strategies. Note that most of these strategies give a similar quality estimate. Strategies that share the initial period have identical behavior for the first approximately 200 individuals sampled.}
\end{figure}

Uniform denotes selecting new bids uniformly from the previously accepted offers. Only one bid is offered to each individual. There is an initial period where offers are given randomly to build up a population of individuals from each private type. Multi-uniform starts by offering each individual a bid selected as in the uniform strategy and continues by randomly offering ($\alpha = 0.5$ probability of making a new offer, stopping otherwise) increasing bids (bids increase by \$1).

Random denotes offers selected uniformly in the range from the minimum (nominally 0) and maximum privacy valuations. Most denotes selecting bids based on previously accepted offers, favoring valuations from the more prevalent private types in the currently learned distribution (more likely accepted bids). Min works similarly, only favoring valuations from the rarest private types (gaining more information). 

Constant 5 denotes offering every individual the same amount less than the maximum valuation (\$5 in this case). This corresponds to an offering strategy similar to that proposed in ``Selling Privacy at Auction''~\cite{Ghosh2011}.

We see that a multi-offer strategy is superior to a single-offer strategy and that the various single-offer strategies give comparable results.

The cumulative cost of each strategy is shown in Figure~\ref{fig:strategy_costs}. The purple line labeled max denotes an upper bound on cost for obtaining all the private information. All the alternative bidding strategies reduce the total cost. However, they make tradeoffs for reduced accuracy.

\begin{figure}
  \centering
  \includegraphics[width=0.7\textwidth]{strategy_costs}
  \caption{\label{fig:strategy_costs} Amount of money spent to sample an increasing number of individuals for different offer strategies. The limiting case of offering all individuals the maximum privacy valuation is shown in purple.}
\end{figure}

A representative learned distribution is compared to the true underlying distribution in Figure~\ref{fig:uniform_offer_learned}. For one of the private types, at least, the learned distribution closely approximates the true distribution. This plot also highlights a common weakness of the learning mechanism -- the variance of the learned distribution may be larger than the underlying distribution. However, the estimate for the mean is unbiased.

\begin{figure}
  \centering
  \includegraphics[width=0.7\textwidth]{uniform_offer_learned}
  \caption{\label{fig:uniform_offer_learned} Comparison of learned (blue) and true underlying distributions (green) of a sample dataset with two private types. The learning mechanism indeed approaches the true distribution, albeit with higher variance.}
\end{figure}


\subsection{Comparison with Other Mechanisms}
Our project manages to differentiate itself from that of Ghosh and Roth in that by learning and adaptively synthesizing a distribution for each private type, we manage to account for the inherent correlation between one's private type and one's valuation of his/her data.  Moreover, unlike Ghosh/Roth and Fleischer/Lyu, our mechanism requires minimal assumptions about the distribution(s) from which individuals' valuations are drawn.  Often, it might not be realistic to be able to find a well-defined distribution prior to sampling the participants, adding an extra level of pragmatism to our learning model.  In fact, while our mechanism can stand alone, it can also serve as a precursor to Fleischer and Lyu's mechanism by efficiently contructing an accurate and unbiased estimator of the true cost distribution from which the appropriate contracts can be drawn up for future survey participants.


\section{Conclusion}
% recap of objectives and results
We have designed and demonstrated a mechanism for determining the distributions of individuals' privacy valuations. This work provides an implementation for realizing many of the approaches previously described in the literature. It gives a researcher a starting point for collecting private data efficiently given minimal prior knowledge of individuals' privacy valuations. Only the number of private types and an upper bound on the payout to any individual need to be known. These assumptions are consistent with previous results.

% To what extent did your group meet its goals?
Figure~\ref{fig:strategy_costs} shows that the bids offered by the learning mechanism are cumulatively less than the bids offered by a naive strategy that offers each individual the same bid. Multiple offer strategies and adaptive strategies also do a fair job of approximating the underlying distributions according to the J-S divergence (Figure~\ref{fig:strategy_log_divergences}). However, the implemented mechanism performs significantly worse than a hypothetical mechanism that knows each individual's valuation and offers them exactly that. However, this is to be expected. Modifications to the learning algorithm, such as to how rejected offers are weighed, may improve accuracy.

% What do you see as the most important remaining open questions in this general area of research? Why?
This general area of research has proceeded by successively relaxing and removing assumptions on individuals' valuations of privacy. Nonetheless, a significant assumption is still the existence of discrete private types. In our model, private types are quite versatile, taking on arbitrary distributions. One can imagine a generalization to this notion wherein there is a global distribution of privacy valuations for all individuals. This would then allow the exploration of the direct relation between private information and private information valuation.


\section*{Credit Assignment}

Arjun Chandrasekhar, Solomon Chang, Bryan He, and Eric Martin contributed primarily to the codebase while Brian Okoro and Kevin Shi contributed primarily to the text of the final report. All members contributed to specifying the offer strategies and their justifications, suggesting implementations for the code, and analyzing the results.

The weekly progress reports have been submitted by the group to the facillator via email.

\bibliographystyle{is-unsrt}
\bibliography{CSEc101}

\section*{Appendix}

The code for this project is hosted at: \href{https://bitbucket.org/bryanhe/value-of-private-information.git}{https://bitbucket.org/bryanhe/value-of-private-information.git}.

\end{document}